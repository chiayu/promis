/**
 *
 * @author Wei-Ming Wu
 *
 *
 * Copyright 2014 Wei-Ming Wu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package gov.nih.promisdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.google.common.testing.EqualsTester;
import com.google.common.testing.NullPointerTester;

public class AssessmentTest {

  Assessment assessment;
  Date expiration;

  @Before
  public void setUp() throws Exception {
    expiration = new Date();
    assessment = new Assessment("OID", "UID", expiration);
  }

  @Test
  public void testConstructor() {
    assertTrue(assessment instanceof Assessment);
  }

  @Test
  public void nullPointerTestOnConstructor() {
    new NullPointerTester().testAllPublicConstructors(Assessment.class);
  }

  @Test
  public void nullPointerTestOnPublicInstanceMethods() {
    new NullPointerTester().testAllPublicInstanceMethods(Assessment.class);
  }

  @Test
  public void checkTheConstructorOIDIsMatchWithTheReturnOfGetOID() {
    assertEquals("OID", assessment.getOID());
  }

  @Test
  public void testEquality() {
    new EqualsTester().addEqualityGroup(assessment, assessment,
        new Assessment("OID", "UID", expiration)).testEquals();
  }

}
