/**
 *
 * @author Wei-Ming Wu
 *
 *
 * Copyright 2014 Wei-Ming Wu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package gov.nih.promisdb;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

public final class PROMISConfig {

  private PROMISConfig() {}

  public static final String API_DOMAIN =
      "https://www.assessmentcenter.net/ac_api/2012-01";

  public static final Type FORM_LIST_TYPE =
      new TypeToken<Map<String, List<Form>>>() {}.getType();

  public static final JsonDeserializer<Date> DATE_FORMAT =
      new JsonDeserializer<Date>() {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");

        @Override
        public Date deserialize(final JsonElement json, final Type typeOfT,
            final JsonDeserializationContext context) throws JsonParseException {
          try {
            return df.parse(json.getAsString());
          } catch (ParseException e) {
            return null;
          }
        }

      };

  public static final FieldNamingStrategy NAMING_STRATEGY =
      new FieldNamingStrategy() {

        @Override
        public String translateName(Field f) {
          if (f.getName().equals("oid"))
            return "OID";
          return f.getName().substring(0, 1).toUpperCase()
              + f.getName().substring(1);
        }

      };

}
