package gov.nih.promisdb;

import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.base.Objects;

@Document
public final class FormOption {

  private final String elementOID;
  private final String description;
  private final String formItemOID;
  private final String itemResponseOID;
  private final String value;
  private final String position;

  public FormOption(String elementOID, String description, String formItemOID,
      String itemResponseOID, String value, String position) {
    this.elementOID = elementOID;
    this.description = description;
    this.formItemOID = formItemOID;
    this.itemResponseOID = itemResponseOID;
    this.value = value;
    this.position = position;

  }

  public String getElementOID() {
    return elementOID;
  }

  public String getDescription() {
    return description;
  }

  public String getFormItemOID() {
    return formItemOID;
  }

  public String getItemResponseOID() {
    return itemResponseOID;
  }

  public String getValue() {
    return value;
  }

  public String getPosition() {
    return position;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(elementOID, description, formItemOID,
        itemResponseOID, value, position);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof FormOption) {
      FormOption participants = (FormOption) o;
      return Objects.equal(participants.getElementOID(), elementOID)
          && Objects.equal(participants.getDescription(), description)
          && Objects.equal(participants.getFormItemOID(), formItemOID)
          && Objects.equal(participants.getItemResponseOID(), itemResponseOID)
          && Objects.equal(participants.getValue(), value)
          && Objects.equal(participants.getPosition(), position);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("ElementOID", elementOID)
        .add("Description", description).add("FormItemOID", formItemOID)
        .add("ItemResponseOID", itemResponseOID).add("Value", value)
        .add("Position", position).toString();
  }

}
