package gov.nih.promisdb;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
public class FormRelationApplication implements CommandLineRunner {

  @Autowired
  private FormRepository formRepo;
  @Autowired
  private FormContentRepository formContentRepo;

  public static void main(String[] args) throws ClientProtocolException,
      IOException {
    SpringApplication.run(FormRelationApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    formRepo.deleteAll();
    formContentRepo.deleteAll();

    PROMIS promis =
        PROMIS.createWithRegistrationIDAndToken(
            "3F4D96AD-C41A-446B-A184-6CC7D2F11C6E",
            "7C245B52-B913-483C-9FC7-E78DC0DCA7B2");

    List<Form> formList = promis.getFormList();
    formRepo.save(formList);

    for (Form form : formRepo.findAll()) {
      FormContent fs = promis.getFormContent(form.getOid());
      form.setFormContent(fs);
      formRepo.save(form);
    }

    for (Form form : formRepo.findAll()) {
      System.out.println(form);
    }
  }

}
