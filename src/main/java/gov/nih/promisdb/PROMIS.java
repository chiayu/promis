/**
 *
 * @author Wei-Ming Wu
 *
 *
 * Copyright 2014 Wei-Ming Wu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package gov.nih.promisdb;

import static gov.nih.promisdb.PROMISConfig.API_DOMAIN;
import static net.sf.rubycollect4j.RubyCollections.newRubyArray;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.common.base.Objects;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class PROMIS {

  private final HttpClient client = HttpClientBuilder.create().build();
  private final Gson gson = new GsonBuilder()
      .registerTypeAdapter(Date.class, PROMISConfig.DATE_FORMAT)
      .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
      .setFieldNamingStrategy(PROMISConfig.NAMING_STRATEGY).create();

  private final String registrationID;
  private final String token;

  public static PROMIS createWithRegistrationIDAndToken(String registrationID,
      String token) {
    return new PROMIS(registrationID, token);
  }

  private PROMIS(String registrationID, String token) {
    this.registrationID = registrationID;
    this.token = token;
  }

  private String formListURL() {
    return newRubyArray(API_DOMAIN, "forms", ".json").join("/");
  }

  private String formURL(String OID) {
    return newRubyArray(API_DOMAIN, "forms", OID + ".json").join("/");
  }

  private String assementURL(String OID) {
    return newRubyArray(API_DOMAIN, "Assessments", OID + ".json").join("/");
  }

  private String participantURL(String OID) {
    return newRubyArray(API_DOMAIN, "Participants", OID + ".json").join("/");
  }

  public List<Form> getFormList() throws ClientProtocolException, IOException {
    HttpPost httpPost = new HttpPost(formListURL());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    Map<String, List<Form>> formMap =
        gson.fromJson(responseToString(response), PROMISConfig.FORM_LIST_TYPE);
    return formMap.get("Form");
  }

  public Assessment createAssessment(Form form, String UID)
      throws ClientProtocolException, IOException, URISyntaxException {
    URIBuilder builder = new URIBuilder(assementURL(form.getOid()));
    builder.setParameter("UID", UID);
    HttpPost httpPost = new HttpPost(builder.build());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return gson.fromJson(responseToString(response), Assessment.class);
  }

  public FormContent getFormContent(String OID) throws URISyntaxException,
      ClientProtocolException, IOException {
    URIBuilder builder = new URIBuilder(formURL(OID));
    builder.setParameter("OID", OID);
    HttpPost httpPost = new HttpPost(builder.build());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return gson.fromJson(responseToString(response), FormContent.class);
  }

  public FormContent participateAssessment(String UID)
      throws ClientProtocolException, IOException {
    HttpPost httpPost = new HttpPost(participantURL(UID));
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return gson.fromJson(responseToString(response), FormContent.class);
  }

  public FormContent answerAssessment(String UID, String ItemResponseOID,
      String FormItemOID) throws ClientProtocolException, IOException,
      URISyntaxException {
    URIBuilder builder = new URIBuilder(participantURL(UID));
    builder.setParameter("ItemResponseOID", ItemResponseOID).setParameter(
        "Response", FormItemOID);
    HttpPost httpPost = new HttpPost(builder.build());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return gson.fromJson(responseToString(response), FormContent.class);
  }

  private String responseToString(HttpResponse res)
      throws IllegalStateException, IOException {
    StringWriter writer = new StringWriter();
    IOUtils.copy(res.getEntity().getContent(), writer);
    return writer.toString();
  }

  private String authstring() {
    return "Basic "
        + Base64.encodeBase64String((registrationID + ":" + token).getBytes());
  }

  public String getRegistrationID() {
    return registrationID;
  }

  public String getToken() {
    return token;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(registrationID, token);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof PROMIS) {
      PROMIS promis = (PROMIS) o;
      return Objects.equal(promis.registrationID, registrationID)
          && Objects.equal(promis.token, token);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("registrationID", registrationID)
        .add("token", token).toString();
  }

}
