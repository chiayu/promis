/**
 *
 * @author Wei-Ming Wu
 *
 *
 * Copyright 2014 Wei-Ming Wu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package gov.nih.promisdb;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import com.google.common.base.Objects;

/**
 * 
 * {@link Assessment} is designed to hold the assessment information from PROMIS
 * API and it's also an entity class of MongoDB.
 *
 */
public final class Assessment {

  private final String OID;
  private final String UID;
  private final Date expiration;

  /**
   * Returns an Assessment.
   * 
   * @param OID
   *          a unique string which represents the ID of PROMIS assessment
   * @param UID
   *          a user defined string which is used to access PROMIS assessment
   * @param expiration
   *          the assessment expiration date
   */
  public Assessment(String OID, String UID, Date expiration) {
    this.OID = checkNotNull(OID);
    this.UID = checkNotNull(UID);
    this.expiration = checkNotNull(expiration);
  }

  /**
   * Returns the unique string which represents the ID of PROMIS assessment.
   * 
   * @return a unique string ID
   */
  public String getOID() {
    return OID;
  }

  public String getUID() {
    return UID;
  }

  public Date getExpiration() {
    return expiration;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(OID, UID, expiration);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Assessment) {
      Assessment assessment = (Assessment) o;
      return Objects.equal(assessment.OID, OID)
          && Objects.equal(assessment.UID, UID)
          && Objects.equal(assessment.expiration, expiration);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("OID", OID).add("UID", UID)
        .add("expiration", expiration).toString();
  }

}
