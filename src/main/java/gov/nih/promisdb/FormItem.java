package gov.nih.promisdb;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.base.Objects;

@Document
public final class FormItem {

  private final String id;
  private final String formItemOID;
  private final String order;
  private final List<FormElement> elements;

  public FormItem(String formItemOID, String id, String order,
      List<FormElement> elements) {
    this.formItemOID = formItemOID;
    this.id = id;
    this.order = order;
    this.elements = elements;
  }

  public String getFormItemOID() {
    return formItemOID;
  }

  public String getID() {
    return id;
  }

  public String getOrder() {
    return order;
  }

  public List<FormElement> getElements() {
    return elements;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(formItemOID, id, order, elements);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof FormItem) {
      FormItem participants = (FormItem) o;
      return Objects.equal(participants.getFormItemOID(), formItemOID)
          && Objects.equal(participants.getID(), id)
          && Objects.equal(participants.getOrder(), order)
          && Objects.equal(participants.getElements(), elements);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("ElementOID", formItemOID)
        .add("ID", id).add("Order", order).add("Elements", elements).toString();
  }

}
