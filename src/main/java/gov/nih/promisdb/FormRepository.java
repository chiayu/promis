package gov.nih.promisdb;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface FormRepository extends MongoRepository<Form, String> {

  public Form findByOid(String oid);

  public Form findByName(String name);

}
