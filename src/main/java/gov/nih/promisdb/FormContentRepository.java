package gov.nih.promisdb;

import java.util.Date;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface FormContentRepository extends
    MongoRepository<FormContent, String> {

  public FormContent findByDateFinished(Date dateFinished);

}
