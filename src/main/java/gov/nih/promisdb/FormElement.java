package gov.nih.promisdb;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.base.Objects;

@Document
public final class FormElement {

  private final String elementOID;
  private final String description;
  private final String elementOrder;
  private final List<FormOption> map;

  public FormElement(String elementOID, String description,
      String elementOrder, List<FormOption> map) {
    this.elementOID = elementOID;
    this.description = description;
    this.elementOrder = elementOrder;
    this.map = map;
  }

  public String getElementOID() {
    return elementOID;
  }

  public String getDescription() {
    return description;
  }

  public String getElementOrder() {
    return elementOrder;
  }

  public List<FormOption> getMap() {
    return map;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(elementOID, description, elementOrder, map);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof FormItem) {
      FormElement participants = (FormElement) o;
      return Objects.equal(participants.getElementOID(), elementOID)
          && Objects.equal(participants.getDescription(), description)
          && Objects.equal(participants.getElementOrder(), elementOrder)
          && Objects.equal(participants.getMap(), map);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("ElementOID", elementOID)
        .add("Description", description).add("ElementOrder", elementOrder)
        .add("Map", map).toString();
  }

}