package gov.nih.promisdb;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.base.Objects;

/*
 * {"DateFinished":"","Items":[{"FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ID":"PAINBE8","Order":"4","Elements":[{"ElementOID":"8AB8BA58-3BB0-40B6-B656-C24F1169069B","Description":"In the past 7 days","ElementOrder":"1"},{"ElementOID":"71C3F674-8B21-4436-AC22-A9F9D7F254E1","Description":"When I was in pain I moved extremely slowly","ElementOrder":"2"},{"ElementOID":"C54EF195-0904-4A9A-9789-510D2F2894A5","Description":"ContainerForC54EF195-0904-4A9A-9789-510D2F2894A5","ElementOrder":"3","Map":[{"ElementOID":"3D86FC4B-516C-42AF-A5EA-028339A0B123","Description":"Had no pain","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"CB09F163-70E3-4BCB-BD7B-45EDC3B2B3FC","Value":"1","Position":"1"},{"ElementOID":"C279A7CB-A5C4-4211-B615-0B1090AE91C8","Description":"Never","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"23FECE02-E4B5-4EFA-BC90-3FCEC808422E","Value":"2","Position":"2"},{"ElementOID":"78A4D40B-4756-4506-AB49-D10EDF187033","Description":"Rarely","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"1D2F0D68-49E2-4DB8-BB66-C3D301AC3947","Value":"3","Position":"3"},{"ElementOID":"6AAC64C7-C474-43CB-9B93-62D14B6978B9","Description":"Sometimes","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"6D7957C7-7F8C-4EEA-A33D-01ABCAEE2130","Value":"4","Position":"4"},{"ElementOID":"C0855A96-447F-472D-8B3B-A51AA4AA86EE","Description":"Often","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"5F7779CF-D102-4C7D-8916-1AE4D0795132","Value":"5","Position":"5"},{"ElementOID":"D1288A7D-DFB4-4D89-B2BE-3C6CE2776F2C","Description":"Always","FormItemOID":"A15654B1-12C1-4EDF-B8D7-496B9DD8ABA0","ItemResponseOID":"20844600-6CF7-4477-A207-1C0219DB5A13","Value":"6","Position":"6"}]}]}]}
 */

@Document
public final class FormContent {

  @Id
  private String id;
  private final Date dateFinished;
  private final List<FormItem> items;

  public FormContent(Date dateFinished, List<FormItem> items) {
    this.dateFinished = dateFinished;
    this.items = items;

  }

  public Date getDateFinished() {
    return dateFinished;
  }

  public List<FormItem> getItems() {
    return items;
  }

  public Date setDateFinished() {
    return dateFinished;
  }

  public List<FormItem> setItems() {
    return items;
  }

  public FormOption getAnswer(int value) {
    List<FormOption> ansers = null;
    for (FormElement elem : items.get(0).getElements()) {
      if (elem.getMap() != null && !elem.getMap().isEmpty()) {
        ansers = elem.getMap();
        break;
      }
    }
    return ansers.get(value);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(dateFinished, items);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof FormContent) {
      FormContent participants = (FormContent) o;
      return Objects.equal(participants.dateFinished, dateFinished)
          && Objects.equal(participants.items, items);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("DateFinished", dateFinished)
        .add("Items", items).toString();
  }

}
