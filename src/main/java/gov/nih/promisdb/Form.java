/**
 *
 * @author Wei-Ming Wu
 *
 *
 * Copyright 2014 Wei-Ming Wu
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package gov.nih.promisdb;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.base.Objects;

@Document
public final class Form {

  @Id
  private String id;
  private final String oid;
  private final String name;
  private FormContent formContent;

  @PersistenceConstructor
  public Form(String oid, String name, FormContent formContent) {
    this.oid = oid;
    this.name = name;
    this.formContent = formContent;
  }

  public String getOid() {
    return oid;
  }

  public String getName() {
    return name;
  }

  public FormContent getFormContent() {
    return formContent;
  }

  public void setFormContent(FormContent formContent) {
    this.formContent = formContent;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(oid, name, formContent);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Form) {
      Form form = (Form) o;
      return Objects.equal(form.oid, oid) && Objects.equal(form.name, name);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("OID", oid).add("name", name)
        .add("formContent", formContent).toString();
  }

}
