package promis;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class ConnectMongoDB {

  public static void main(String... args) throws ClientProtocolException,
      IOException {
    PROMIS promis = PROMIS.createWithRegistrationIDAndToken(
        "27F84718-8ED2-4419-8959-22FDDEF984CE",
        "7EDFB8E4-F0F1-46D0-AF56-7795F95E136C");
    List<Form> formList = promis.getFormList();

    MongoClient mongoClient = new MongoClient("localhost", 27017);

    System.out.println(formList.get(0));
    DB db = mongoClient.getDB("FormData");

    mongoClient.close();
    // db.getCollection("mycollection").insert(promis.getStringFormList());
  }

}
