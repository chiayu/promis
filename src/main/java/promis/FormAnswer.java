package promis;

import com.google.common.base.Objects;
import com.google.gson.JsonArray;

public final class FormAnswer {

  private final String DateFinished;
  private final JsonArray Items;

  public FormAnswer(String DateFinished, JsonArray Items) {
    this.DateFinished = DateFinished;
    this.Items = Items;
  }

  private JsonArray getMap() {

    JsonArray map = new JsonArray();
    for (int i = 0; i < Items.get(0).getAsJsonObject()
        .getAsJsonArray("Elements").size(); i++) {

      if (Items.get(0).getAsJsonObject().getAsJsonArray("Elements").get(i)
          .getAsJsonObject().getAsJsonArray("Map") != null) {
        map = Items.get(0).getAsJsonObject().getAsJsonArray("Elements").get(i)
            .getAsJsonObject().getAsJsonArray("Map");
      }
    }
    return map;
  }

  public String getDateFinished() {
    return DateFinished;
  }

  public JsonArray getItems() {
    return Items;
  }

  public void showItems() {

    String answer = getMap().get(0).getAsString();
    System.out.println(answer);
  }

  public void showQuestion() {
    String ques = Items.get(0).getAsJsonObject().getAsJsonArray("Elements")
        .get(0).getAsJsonObject().get("Description").getAsString()
        + Items.get(0).getAsJsonObject().getAsJsonArray("Elements").get(1)
            .getAsJsonObject().get("Description").getAsString();
    System.out.println(ques);
  }

  public void showMyAnswer(int INDEX) {

    String des = getMap().get(INDEX).getAsJsonObject().get("Description")
        .getAsString();
    System.out.println("Answer: " + des);
  }

  public void showAnswerChoices() {

    for (int j = 0; j < getMap().size(); j++) {
      // System.out.println(map.get(j).getAsJsonObject().get("ItemResponseOID"));
      System.out.println(getMap().get(j).getAsJsonObject().get("Description"));
      // System.out.println(map.get(j).getAsJsonObject().get("Value"));
      // System.out.println(map.get(j).getAsJsonObject().get("ElementOID"));
    }

  }

  public String getFormItemOID() {
    // System.out.println(Items.get(0).getAsJsonObject().get("FormItemOID").toString());
    return Items.get(0).getAsJsonObject().get("FormItemOID").getAsString();
  }

  public int getQuestionsNum() {
    return getMap().size();
  }

  public String getItemResponseOID(int INDEX) {

    return getMap().get(INDEX).getAsJsonObject().get("ItemResponseOID")
        .getAsString();

  }

  @Override
  public int hashCode() {
    return Objects.hashCode(DateFinished, Items);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof FormAnswer) {
      FormAnswer formanswer = (FormAnswer) o;
      return Objects.equal(formanswer.DateFinished, DateFinished)
          && Objects.equal(formanswer.Items, Items);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("DateFinished", DateFinished)
        .add("Items", Items).toString();
  }
}
