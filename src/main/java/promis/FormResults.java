package promis;

import com.google.gson.JsonArray;

public class FormResults {

  private final String UID;
  private final String Name;
  private final JsonArray Items;

  public FormResults(String UID, String Name, JsonArray Items) {
    this.UID = UID;
    this.Name = Name;
    this.Items = Items;
  }

  public String getUID() {
    return UID;
  }

  public String getName() {
    return Name;
  }

  public void showScores() {
    for (int i = 0; i < Items.size(); i++) {

      System.out.println("FormItemOID: "
          + Items.get(i).getAsJsonObject().get("FormItemOID").getAsString());
      System.out.println("\tStdError: "
          + Items.get(i).getAsJsonObject().get("StdError").getAsString());
      System.out.println("\tTheta: "
          + Items.get(i).getAsJsonObject().get("Theta").getAsString());
    }
  }
}
