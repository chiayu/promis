package promis;

import java.util.Date;

import com.google.common.base.Objects;

public final class Assessment {

  private final String OID;
  private final String UID;
  private final Date expiration;

  public Assessment(String OID, String UID, Date expiration) {
    this.OID = OID;
    this.UID = UID;
    this.expiration = expiration;
  }

  public String getOID() {
    return OID;
  }

  public String getUID() {
    return UID;
  }

  public Date getExpiration() {
    return expiration;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(OID, UID, expiration);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Assessment) {
      Assessment assessment = (Assessment) o;
      return Objects.equal(assessment.OID, OID)
          && Objects.equal(assessment.UID, UID)
          && Objects.equal(assessment.expiration, expiration);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("OID", OID).add("UID", UID)
        .add("expiration", expiration).toString();
  }

}
