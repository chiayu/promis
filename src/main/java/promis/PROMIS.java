package promis;

import static net.sf.rubycollect4j.RubyCollections.newRubyArray;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.common.base.Objects;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public final class PROMIS {

  private static final String DOMAIN = "https://www.assessmentcenter.net/ac_api/2012-01";

  private static final Type FORM_LIST_TYPE = new TypeToken<Map<String, List<Form>>>() {
  }.getType();

  private final HttpClient client = HttpClientBuilder.create().build();
  private final Gson gson = new GsonBuilder()
      .setDateFormat("MM/dd/yyyy hh:mm:ss aa")
      .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

  private final String registrationID;
  private final String token;

  public static PROMIS createWithRegistrationIDAndToken(String registrationID,
      String token) {
    return new PROMIS(registrationID, token);
  }

  private PROMIS(String registrationID, String token) {
    this.registrationID = registrationID;
    this.token = token;
  }

  private String formListURL() {
    return newRubyArray(DOMAIN, "forms", ".json").join("/");
  }

  private String assementURL(String OID) {
    return newRubyArray(DOMAIN, "Assessments", OID + ".json").join("/");
  }

  private String resultScoreURL(String AID) {
    return newRubyArray(DOMAIN, "Results", AID + ".json").join("/");
  }

  private String participantURL(String OID) {
    return newRubyArray(DOMAIN, "Participants", OID + ".json").join("/");
  }

  public List<Form> getFormList() throws ClientProtocolException, IOException {
    HttpPost httpPost = new HttpPost(formListURL());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    Map<String, List<Form>> formMap = gson.fromJson(responseToString(response),
        FORM_LIST_TYPE);
    return formMap.get("Form");
  }

  public Assessment createAssessment(Form form, String UID)
      throws ClientProtocolException, IOException, URISyntaxException {
    URIBuilder builder = new URIBuilder(assementURL(form.getOID()));
    builder.setParameter("UID", UID);
    HttpPost httpPost = new HttpPost(builder.build());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    // System.out.println(responseToString(response));
    return gson.fromJson(responseToString(response), Assessment.class);
  }

  public String participateAssessment(String UID)
      throws ClientProtocolException, IOException {
    HttpPost httpPost = new HttpPost(participantURL(UID));
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return responseToString(response);
  }

  public FormAnswer answerAssessment(String UID, String ItemResponseOID,
      String FormItemOID) throws ClientProtocolException, IOException,
      URISyntaxException {

    URIBuilder builder = new URIBuilder(participantURL(UID));
    builder.setParameter("ItemResponseOID", ItemResponseOID).setParameter(
        "Response", FormItemOID);
    HttpPost httpPost = new HttpPost(builder.build());
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);

    return gson.fromJson(responseToString(response), FormAnswer.class);

  }

  public FormResults retrieveAssessmentScore(String AID)
      throws ClientProtocolException, IOException {

    HttpPost httpPost = new HttpPost(resultScoreURL(AID));
    httpPost.addHeader("Authorization", authstring());
    HttpResponse response = client.execute(httpPost);
    return gson.fromJson(responseToString(response), FormResults.class);

  }

  private String responseToString(HttpResponse res)
      throws IllegalStateException, IOException {
    StringWriter writer = new StringWriter();
    IOUtils.copy(res.getEntity().getContent(), writer);
    return writer.toString();
  }

  private String authstring() {
    return "Basic "
        + Base64.encodeBase64String((registrationID + ":" + token).getBytes());
  }

  public String getRegistrationID() {
    return registrationID;
  }

  public String getToken() {
    return token;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(registrationID, token);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof PROMIS) {
      PROMIS promis = (PROMIS) o;
      return Objects.equal(promis.registrationID, registrationID)
          && Objects.equal(promis.token, token);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("registrationID", registrationID)
        .add("token", token).toString();
  }

  public void fillTheForm(PROMIS promis, String aid)
      throws ClientProtocolException, IOException, URISyntaxException {
    FormAnswer ansAssessment = promis.answerAssessment(aid, "", "");

    // randomize
    int i = 0;

    while (ansAssessment.getDateFinished().isEmpty()) {
      i = (int) (Math.random() * ansAssessment.getQuestionsNum());

      ansAssessment.showQuestion();
      ansAssessment.showAnswerChoices();
      ansAssessment.showMyAnswer(i);

      ansAssessment = promis.answerAssessment(aid,
          ansAssessment.getItemResponseOID(i), ansAssessment.getFormItemOID());
    }

  }

  public static void main(String... args) throws ClientProtocolException,
      IOException, URISyntaxException {
    PROMIS promis = PROMIS.createWithRegistrationIDAndToken(
        "27F84718-8ED2-4419-8959-22FDDEF984CE",
        "7EDFB8E4-F0F1-46D0-AF56-7795F95E136C");
    List<Form> formList = promis.getFormList();

    // System.out.println(formList);

    String aid = promis.createAssessment(formList.get(0), "chia-yu").getOID();
    System.out.println(aid);

    promis.fillTheForm(promis, aid);

    System.out.println("========= Done fill the Form ==========");

    promis.retrieveAssessmentScore(aid).showScores();

  }

}