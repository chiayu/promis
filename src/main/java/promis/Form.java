package promis;

import com.google.common.base.Objects;

public final class Form {

  private final String OID;
  private final String name;

  public Form(String OID, String name) {
    this.OID = OID;
    this.name = name;
  }

  public String getOID() {
    return OID;
  }

  public String getName() {
    return name;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(OID, name);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Form) {
      Form form = (Form) o;
      return Objects.equal(form.OID, OID) && Objects.equal(form.name, name);
    }
    return false;
  }

  @Override
  public String toString() {
    return Objects.toStringHelper(this).add("OID", OID).add("name", name)
        .toString();
  }

}